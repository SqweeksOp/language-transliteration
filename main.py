# Importing essential libraries
from flask import Flask, render_template, request
import pickle
from keras.models import model_from_json
import os
from keras_preprocessing.sequence import pad_sequences
import numpy as np


base_dir = r""
model_name="English_To_Hindi"
max_len=40


#char encoder
with open(os.path.join(base_dir,"model",model_name+"_eng2index.pickle"),"rb") as file:
    eng2index=pickle.load(file)

with open(os.path.join(base_dir,"model",model_name+"_hindi2index.pickle"),"rb") as file:
    hindi2index=pickle.load(file)
    


#char decoder
index2hindi=dict((i,char) for char,i in hindi2index.items())
index2eng=dict((i,char) for char,i in eng2index.items())



#model loading
with open(os.path.join(base_dir,"model",model_name+"_json.json"),"r") as file:
    json_str=file.read()
model=model_from_json(json_str)

weight_path=os.path.join(base_dir,"model",model_name+"_weights.h5")
model.load_weights(weight_path)



app = Flask(__name__)

@app.route('/')
def home():
	return render_template('home.html')

@app.route('/predict',methods=['POST'])
def predict():
	if request.method == 'POST':
		message = request.form['message']
		texts = [message]
		char_seq_list=[list(text.lower().strip()) for text in texts]

		encoded_seq_list=[[eng2index[char]  for char in char_seq] for char_seq in char_seq_list]
		padded_seq=pad_sequences(encoded_seq_list,maxlen=max_len,padding="post",value=eng2index[" "])
		pred=model.predict(padded_seq)

		dic={}
		dic["English"]=[]
		dic["Hindi"]=[]
		dic["Predicted"]=[]
		for wordSeq,prob in zip(encoded_seq_list,pred):
			hi=""
			eng=""
			prob = np.argmax(prob, axis=-1)
			for w, pred in zip(wordSeq, prob):
				eng = eng + index2eng[w]
				hi=hi+index2hindi[pred]
			dic["English"].append(eng)
			dic["Predicted"].append(hi)
			

	return render_template('result.html',prediction = hi)
		
		
if __name__ == '__main__':
	app.run(debug=True)
